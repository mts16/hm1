def same_strings(string1: str, string2: str) -> bool:
    if len(string1) != len(string2):
        return False

    for elem in string1:
        if string2.find(elem) < 0:
            return False

    return True


if __name__ == "__main__":
    assert same_strings("foo", "oof")
    assert not same_strings("foo", "bar")
    assert not same_strings("foo", "ofoo")
