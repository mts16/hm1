from ast import Str


def archiver(string: str) -> str:
    result: list[str] = []

    current_element = ''
    repetiotion_count = 1

    for char in string:
        if char == current_element:
            repetiotion_count += 1
        else:
            if repetiotion_count > 1:
                result.append(str(repetiotion_count))

            current_element = char
            result.append(current_element)
            repetiotion_count = 1

    if repetiotion_count > 1:
        result.append(str(repetiotion_count))
    
    return "".join(result)


if __name__ == "__main__":
    assert archiver("AAABBBBCCC") == "A3B4C3"
    assert archiver("AAAAAA") == "A6"
    assert archiver("ABBBBCDE") == "AB4CDE"
    assert archiver("AABBBA") == "A2B3A"
