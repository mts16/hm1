import functools
from inspect import BoundArguments, Signature, signature


def types(func):
    
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        sig: Signature = signature(func)
        
        for argument, value in sig.bind(*args, **kwargs).arguments.items():
            annotation = sig.parameters[argument].annotation
            actualy_type = type(value)

            if not isinstance(value, annotation):
                raise TypeError(f"expected: {annotation}, actualy: {actualy_type}, value: {value}")

        return func(*args, **kwargs)
    
    return wrapper


@types
def f(a: int, b: bool, c: str, d: float):
    return a, b, c, d


if __name__ == "__main__":
    f(5, True, 'Ahmed', 4.2)  # ok
    f("5", True, 'Ahmed', 4.2)  # error
    f(5, 1, 'Ahmed', 4.2)  # error
    f(5, True, [1, 2, 3], 4.2)  # error
    f(5, True, 'Ahmed', 4)  # error
