from typing import List


def max_profit(prices: List[int]) -> int:
    prof = 0
    buy_idx = prices[0]

    for price in prices:
        if price < buy_idx and prof == 0:
            buy_idx = price

        if price > prof and price != buy_idx:
            prof = price
    
    if prof == 0 or buy_idx >= prof:
        return 0
    
    return prof - buy_idx


if __name__ == "__main__":
    assert max_profit([7, 1, 5, 3, 6, 4]) == 5
    assert max_profit([7, 6, 4, 3, 1]) == 0
