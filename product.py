from typing import List

def product_except_self(nums: List[int]) -> List[int]:
    zero_count = 0
    nums_multiple = 1

    for num in nums:
        if zero_count >= 2:
            break

        if num == 0:
            zero_count += 1
        else:
            nums_multiple *= num

    if zero_count >= 2:
        return [0] * len(nums)

    if zero_count == 1:
        return  [nums_multiple if num == 0 else 0 for num in nums]

    return [int(nums_multiple / num) for num in nums]


if __name__ == "__main__":
    assert product_except_self([1, 2, 3]) == [6, 3, 2]
    assert product_except_self([1, 0, 3]) == [0, 3, 0]
    assert product_except_self([0, 0, 3]) == [0, 0, 0]
