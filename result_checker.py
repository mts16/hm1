import functools
from typing import List


def result_checker(func=None, *, checker = lambda x: x > 0):
    if func is None:
        return functools.partial(result_checker, checker=checker)

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)

        if checker(result):
            return result

        raise ValueError("Not valid error")
    
    return wrapper


@result_checker(checker=lambda x: x > 0)
def scoring(x: int) -> int:
    return -5 + 2 * x


@result_checker(checker=lambda x: 5 in x)
def concat(a: int, b: int, c: int) -> List[int]:
    return [a, b, c]


@result_checker(checker=lambda x: sum(x) == 1)
def prob(x: List[str], y: int) -> List[float]:
    return [i / y for i in x]


if __name__ == "__main__":
    # 1 example
    print(scoring(4))  # ok
    print(scoring(1))  # error

    # 2 example
    print(concat(1, 2, 5))  # ok
    print(concat(1, 2, 3))  # error

    # 3 example
    print(prob([2, 2], 4))  # ok
    print(prob([3, 2], 4))  # error
